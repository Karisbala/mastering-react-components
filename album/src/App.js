import './App.css';
import Footer from './components/Footer'
import Main from './components/Main'
import Header from './components/Header'
import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';

function App() {

  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/photos?_limit=10')
      .then((response) => response.json())
      .then((data) => setPhotos(data));
  }, []);

  return (
    <>
      <Header title="Header title" />
      <Main photos={photos} />
      <Footer title="Footer title" />
    </>
  );
}

export default App;
