import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Photo from './Photo';

function Main({ photos }) {
    return (
        <main>
            <h2 className="mt-4 mb-3">Photos</h2>
            <div className="row">
                {photos.map((photo) => (
                    <Fragment key={photo.id}>
                        <div className="col-md-6 col-lg-4">
                            <Photo id={photo.id} title={photo.title} url={photo.url}>
                                <p>Photo number {photo.id}</p>
                            </Photo>
                        </div>
                    </Fragment>
                ))}
            </div>
        </main>
    );
}

Main.propTypes = {
    photos: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired,
        })
    ).isRequired,
};

export default Main;
