import PropTypes from 'prop-types';

function Photo({ id, title, url, children }) {
    return (
        <div className="card mb-3">
            <img src={url} alt={title} className="card-img-top" />
            <div className="card-body">
                <h5 className="card-title">{title}</h5>
                {children}
            </div>
        </div>
    );
}

Photo.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    children: PropTypes.node,
};

export default Photo;
