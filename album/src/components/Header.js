import React from 'react';
import PropTypes from 'prop-types';

function Header({ title }) {
    return <header className="bg-dark text-white p-3 w-100">{title}</header>;
}

Header.propTypes = {
    title: PropTypes.string,
};

Header.defaultProps = {
    title: 'Header title',
};

export default Header;
