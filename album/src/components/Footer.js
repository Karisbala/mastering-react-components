import React from 'react';
import PropTypes from 'prop-types';

function Footer({ title }) {
    return <footer className="bg-dark text-white p-3 w-100">{title}</footer>;
}

Footer.propTypes = {
    title: PropTypes.string,
};

Footer.defaultProps = {
    title: 'Footer title',
};

export default Footer;
